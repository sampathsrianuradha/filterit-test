package com.swivel;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        desktop();
    }

    public static void desktop(){
        Scanner enter = new Scanner(System.in);
        printOut("Select search option and enter: ");
        printOut("Type 1 to search ");
        printOut("type 2 to exit ");
        String type = enter.next();
        switch(type) {
            case "1":
                searchScreen();
                break;
            case "2":
               exit();
                break;
        }
    }

    public static void searchScreen(){
        Scanner enter = new Scanner(System.in);
        printOut("Select data set to search: ");
        printOut("Type 1 to organization");
        printOut("type 2 to users");
        printOut("type 3 to tickets");
        printOut("type 4 to exit ");
        String type = enter.next();
        int typeInt = Integer.parseInt(type);
        if ((typeInt > 0 ) && (typeInt < 5)) {
           if( typeInt != 4 ) {
               printOut("Please type search term and hit enter");
               String searchKeyword = enter.next();
               search(searchKeyword, typeInt);
           } else {
               exit();
           }
        }

    }

    public static void search(String text, Integer type){
        printOut("Searching ... Please wait...");
    }

    private static void printOut(String text){
        System.out.println(text);
    }
    private static void exit(){
        System.exit(0);
    }

}
